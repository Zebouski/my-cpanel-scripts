# My cPanel Scripts
## Usage
### Shell Scripts
These are tested with and should be used with Bash 4.2 or later.

Clone this git repo, or wget the hosted blob, then execute it:
```
/bin/bash shell-script.sh
```

### Python Scripts
Currently only tested on Python 2.7,

Todo: Python 3 and pip requirements.txt

#### Zip File Method
If you already have a Python installation with the prequisite libraries, download the .zip [from the releases page](https://gitlab.com/Zebouski/my-cpanel-scripts/-/releases):
```
wget https://gitlab.com/Zebouski/my-cpanel-scripts/uploads/77e50a7c4d9387716be47e4bab2e5f9a/largefinder2.zip
```

Run the zip with a specific python installation, add -h for program usage:
```
/path/to/python2.7 python-script.zip
/path/to/python2.7 python-script.zip -h
```

##### Building
Instead of using the official releases, you can modify and build it yourself.

Clone the repo, and in the directory:
```
make
```

This will use package_app.sh to create a zip per each app, as per Makefile. The .zip files will be stored in ./bin/, and can be ran:
```
/path/to/python2.7 ./bin/python-script.zip
/path/to/python2.7 ./bin/python-script.zip -h
```

#### Pip Method
Coming Soon
