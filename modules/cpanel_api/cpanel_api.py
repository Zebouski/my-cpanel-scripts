"""Interface with cPanel's whmapi and uapi"""
import json
import yaml
import sys
import logging
from getpass import getuser
import subprocess
import shlex

logging.getLogger(__name__)

class API():
    def __init__(self):
        self.current_user = getuser()

    def call(self, api, cmd='', user=''):
        """Call and return from cPanel's APIs"""

        # Sanity checks
        accepted_apis = (
            "whmapi1",
            "uapi",
            "cpapi2"
            )
        if api not in accepted_apis:
            logging.critical('invalid api type %s' % api)
            sys.exit(1)
        if api == 'whmapi1' and self.current_user != 'root':
            logging.critical(
                'WHMAPI1 commands must be run as root.')
            sys.exit(1)

        # Set up the call's args
        api_command = [
            api,
            cmd,
            '--output=json'
            ]
        if api == 'uapi' and self.current_user == 'root':
            # uapi as root needs to specify which cPanel username to operate on
            api_command.append('--user=' + user)

        # Perform the call
        terminal_command = " ".join(api_command)
        logging.debug("Running API command: %s", terminal_command)
        try:
            api_return, api_command_error = subprocess.Popen(
                shlex.split(terminal_command),
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
                ).communicate()
        except:
            logging.exception('cPanel API subprocess open failed!')
            sys.exit(1)

        # check the call
        logging.debug("Investigating the %s return...", api)
        if api_command_error == '':
            api_return = json.loads(api_return)

            self.check_api_return_for_issues(api_return, api)

            if api == "whmapi1":
                return api_return['data']
            elif api == "uapi":
                return api_return['result']['data']
            elif api == "cpapi2":
                return api_return['cpanelresult']['data']
            else:
                return api_return
        else:
            logging.critical(
                '%s Command Failed to Run, stderr: %s',
                api, api_command_error)
            sys.exit(1)

    def check_api_return_for_issues(self, api_return, api):
        """Log if exceptions occur, else do nothing"""
        if api == "whmapi1":
            if api_return['metadata']['version'] != 1:
                logging.warning(
                    "This cPanel API module is not tested with whmapi version %s, expected 1 instead, exiting",
                    api_return['metadata']['version'])
            if api_return['metadata']['result'] != 1:
                logging.warning(
                    "whmapi1 returned error flag                            with this reason: %s",
                    api_return['metadata']['reason'])
        elif api == "uapi":
            if api_return['apiversion'] != 3:
                logging.warning(
                    "This cPanel API module is not tested with uapi version %s, expected 3 instead.",
                    api_return['apiversion'])
            if api_return['result']['errors'] is not None:
                logging.info(
                    "uapi returned this error: %s",
                    '\n'.join(api_command_error for api_command_error in api_return['result']['errors']))
            if api_return['result']['messages'] is not None:
                logging.warning(
                    "uapi returned this message: %s",
                    '\n'.join(
                        message for message
                        in api_return['result']['messages']))
            if api_return['result']['warnings'] is not None:
                logging.warning(
                    "uapi returned this warning: %s",
                    '\n'.join(
                        warning for warning
                        in api_return['result']['warnings']))
        elif api == "cpapi2":
            if api_return['cpanelresult']['apiversion'] != 2:
                logging.warning(
                    "This cPanel API module is not tested with cpapi2 version %s, expected 2 instead.",
                    api_return['cpanelresult']['apiversion'])
            logging.info(api_return)

            # todo: account for this kind of return too
            """cpanelresult:
            apiversion: 2
            data:
                -
                result:
                    status: 0
                    statusmsg: You do not possess permission to read the zone for “”."""
        else:
            logging.critical("Unrecognized api, can't check.")

        logging.debug("%s return passed all defined checks", api)
