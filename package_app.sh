orig_dir="$PWD"
export_zip="${orig_dir}/bin/$1.zip"

rm -v "${export_zip}"

cd ${orig_dir}/apps/$1
zip -r -j ${export_zip} *

cd ${orig_dir}/modules/
zip -r ${export_zip} $2

echo "Packaged to: ${export_zip}"
