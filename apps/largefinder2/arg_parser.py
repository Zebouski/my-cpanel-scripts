from argparse import ArgumentParser

def arg_parser():
    parser = ArgumentParser(
        description='A tool to help determine where to check for large files and folders, so that we can help cleanup high disk usage.',
        prog="largefinder2.py")
    parser.add_argument(
        "--devices",
        action="store_true",
        help="usage of servers devices, its df -h but less cruft")
    parser.add_argument(
        "--users",
        action="store_true",
        help="basically WHM List Accounts, when you sort by Disk Used, but also make sure there isn't anything extra taking up space in the /home*/ directories")
    parser.add_argument(
        "folders",
        type=str,
        nargs='*',
        help="prints disk usage within each folder recursively, but with non-recursive folder sizing")
    parser.add_argument(
        "-t",
        "--threshold",
        type=int,
        default=100000000,
        help="omit files/folders smaller than this value in bytes, defaults to 100MB")

    # Logging
    parser.add_argument(
        '-V', '--verbose',
        action='store_true',
        help='Enable Verbose Display')

    parser.add_argument(
        '--debug',
        action='store_true',
        help='Enable Debug Display')

    parser.add_argument(
        '--quiet',
        action='store_true',
        help='Suppress all display except Error and Critical')

    return parser.parse_args()
