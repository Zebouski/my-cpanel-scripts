import json
import subprocess
import os
import logging

import cpanel_api

api = cpanel_api.cpanel_api.API()

class Checker():
    def __init__(self, threshold):
        self.logger = logging.getLogger(
            '__main__.checker.Checker')

        if os.geteuid() != 0:
            logging.critical("This script makes use of WHM's API which only runs as root. Aborting.")
            sys.exit(1)

        self.threshold = threshold

    def __human_readable(self, num, suffix='B'):
        """We work in bytes throughout this script, this fucntion is used to convert to human readable strings for output"""
        for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti']:
            if abs(num) < 1024.0:
                return "%3.1f%s%s" % (num, unit, suffix)
            num /= 1024.0

    def usage_per_device(self):
        """Retrieves and prints devices/disks on the server and their used/free/total"""
        devices = api.call("whmapi1", "getdiskusage")['partition']

        def __kib_to_b(kibibytes):
            """Used to convert from whmapi's kibibytes to bytes"""
            return kibibytes * 1024

        device_usage = {
            "headers":
                (
                    'Used',
                    'Mount',
                    'Free',
                    'Total',
                    'Used%'
                ),
            "data":
                [
                    [
                        __kib_to_b(device['used']),
                        device['mount'],
                        __kib_to_b(device['available']),
                        __kib_to_b(device['total']),
                        device['percentage']
                    ] for device in devices
                        if "tmp" not in device['mount']
                ]
            }

        # to Human Readable, for "Used, Available, Total" columns
        for column in [0, 2, 3]:
            for place, value in enumerate([i[column] for i in device_usage['data']]):
                device_usage['data'][place][column] = self.__human_readable(value)

        return device_usage

    def cpanel_users(self):
        """Retrieves cPanel user list, sorts them by reported disk size, and uses this list to find extraneous homedir files/folders"""
        def __mib_to_b(mebibytes):
            """whmapi listaccts gets us disk usage in MiB plus "M" making it a string, this strips the M and converts to integer"""
            return int(mebibytes[:-1]) * 1024 * 1024

        cpanel_users = api.call(
            "whmapi1",
            "listaccts",
            ["want=diskused,user,disklimit,partition"])['acct']

        users_usage = {
            "headers":
                (
                    'Used',
                    'User',
                    'Quota',
                    'Partition',
                ),
            "data":
                [
                    [
                        __mib_to_b(cpanel_user['diskused']),
                        cpanel_user['user'],
                        cpanel_user['disklimit'],
                        "/" + cpanel_user['partition'] + "/"
                    ] for cpanel_user in cpanel_users
                ]
            }
        users_usage['data'].sort(key=lambda a: a[0], reverse=True)

        # build list of homedirs
        homedirs = [user[3] for user in users_usage['data']]
        # remove dupliates from list
        homedirs = list(dict.fromkeys(homedirs))
        # now sort them alphabetically
        homedirs.sort()
        logging.debug("Found homedirs: %s", homedirs)

        # filter out users below threshold
        user_count = len(users_usage['data'])
        users_usage['data'] = [
            user_usage for user_usage in users_usage['data'] if \
                user_usage[0] > self.threshold
        ]
        logging.info("%s users omitted (smaller than %s threshold)",
                     user_count - len(users_usage['data']),
                     self.__human_readable(self.threshold))

        # to Human Readable, for "Used" column
        for place, value in enumerate([i[0] for i in users_usage['data']]):
            users_usage['data'][place][0] = self.__human_readable(value)


        homedirs_users = {homedir:[
                user[1] for user in users_usage['data']
                if user[3] == homedir
            ] for homedir in homedirs}

        cpanel_database_api = api.call("whmapi1",
                                       "list_databases")['payload']
        mysql_cpanel_databases = [database['name'] for database
                            in cpanel_database_api]

        try:
            mysql_datadir = subprocess.Popen(
                "mysql -s -N -e 'select @@datadir'",
                shell=True,
                stdout=subprocess.PIPE
                ).stdout.read().strip()
        except:
            mysql_datadir = "/var/lib/mysql/"
            logging.warning("Unable to confirm MySQL data directory. Assuming %s" % mysql_datadir)

        return users_usage, homedirs_users, mysql_datadir, mysql_cpanel_databases


    def __process_extraneous(self, directory, fsos, whitelisted_fsos):
        logging.debug("Subtracting: %s ... from: %s", whitelisted_fsos, fsos)
        # elimiate whitelisted filesystem objects from the list
        extraneous = [fso for fso in fsos
                      if not fso.startswith(whitelisted_fsos)]

        # build full OS path
        extraneous = [directory + fso for fso in extraneous]

        # flatten to a signle string
        extraneous = " ".join(extraneous)
        return extraneous

    def cpanel_home_extraneous(self, homedir, userdirs):
        logging.debug("The %s homedir houses these users: %s", homedir, userdirs)

        ls = os.listdir(homedir)

        important_folders = ("virtfs",)
        whitelisted_fsos = important_folders + tuple(userdirs)

        extraneous = self.__process_extraneous(homedir, ls,
                                               whitelisted_fsos)
        return extraneous

    def cpanel_mysql_extraneous(self, mysql_datadir, mysql_cpanel_databases):
        ls = os.listdir(mysql_datadir)


        important_files = (
            'tc.log',
            'ib',
            'aria'
            )
        root_dbs = (
            'cphulkd',
            'mysql',
            'roundcube',
            'leechprotect',
            'performance_schema',
            'modsec'
            )
        whitelisted_fsos = important_files \
            + root_dbs \
            + tuple(mysql_cpanel_databases)

        extraneous = self.__process_extraneous(mysql_datadir, ls,
                                               whitelisted_fsos)
        return extraneous

    # usage for each path passed as argument
    def usage_within_folder(self, what_to_du):
        """Wraps around du util to check for large folders and files"""
        cmd = 'du -cabS ' + str(what_to_du) + ' | sort -hr'
        logging.debug("Running in shell: %s", cmd)
        folder_du = subprocess.Popen(
            cmd,
            shell=True,
            stdout=subprocess.PIPE
            ).stdout.read()

        folder_usage = {
            'data': [
                subfolder_usage.split() for subfolder_usage in folder_du.splitlines()
                ]
            }

        orig_item_count = len(folder_usage['data'])
        # filter out small folders
        folder_usage['data'] = [
            subfolder_usage for subfolder_usage in folder_usage['data'] if str(subfolder_usage[1]) == "total" or int(subfolder_usage[0].strip()) > self.threshold
        ]
        logging.info("%s files/folders omitted (smaller than %s threshold)",
                     orig_item_count - len(folder_usage['data']),
                     self.__human_readable(self.threshold))

        # to Human Readable, for "Used" column
        for place, value in enumerate([i[0] for i in folder_usage['data']]):
            folder_usage['data'][place][0] = self.__human_readable(int(value))

        return folder_usage
