'''A tool to help determine where to check for large files and folders, so that we can help cleanup high disk usage.'''
import os
import logging
import sys

import arg_parser
import checker
import view

logging.basicConfig(
    format='[ %(levelname)s ] %(message)s ',
    stream=sys.stdout,
    )
logger = logging.getLogger()

args = arg_parser.arg_parser()
if args.debug:
    logger.setLevel('DEBUG')
if args.verbose:
    logger.setLevel('INFO')
if args.quiet:
    logger.setLevel('ERROR')
logging.debug("Parsed Args: %s", args)

checker = checker.Checker(args.threshold)

view.title("LARGE FILE/FOLDER LOCATOR 2: ELECTRIC BOOGALOO")

if args.devices:
    view.title("Usage per Device:")
    view.table(checker.usage_per_device())
else:
    view.title("--devices flag missing, skipping that...")

if args.users:
    view.title("Usage per cPanel Account:")

    users_usage, homedirs_users, mysql_datadir, mysql_cpanel_databases = checker.cpanel_users()

    view.table(users_usage)

    # check for extraneous files/folders for each homedir
    for homedir in homedirs_users:
        view.title("Usage of extraneous data within " + homedir)
        extraneous = checker.cpanel_home_extraneous(
            homedir, homedirs_users[homedir])
        view.table(checker.usage_within_folder(extraneous))

    # check /var/lib/mysql/
    view.title("Usage of extraneous data within "
        + mysql_datadir)
    extraneous = checker.cpanel_mysql_extraneous(mysql_datadir,
                                                 mysql_cpanel_databases)
    view.table(checker.usage_within_folder(extraneous))

else:
    view.title("--users flag missing, skipping that...")

for folder in args.folders:
    view.title("Usage within " + folder)
    view.table(checker.usage_within_folder(folder))
