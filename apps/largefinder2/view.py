from tabulate import tabulate

def title(title):
    print("\n" + title)

def table(table):
    try:
        print(tabulate(table['data'],
                       headers=table['headers']))
    except KeyError:
        print(tabulate(table['data']))