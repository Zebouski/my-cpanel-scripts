import sys
import logging
from getpass import getuser

import cpanel_api

class MultiPHP(object):
    def __init__(self, domains):
        self.api = cpanel_api.cpanel_api.API()
        self.domains = domains
        self.current_user = getuser()
        self.is_root = True if self.current_user == "root" else False
        logging.info("Root access? %s", self.is_root)

        self.users_owned_domains = self.generate_users_owned_domains()

        # "check all domains" was not given, so we must prune
        if isinstance(self.domains, list):
            self.users_owned_domains = self.prune_users_owned_domains(
                    self.users_owned_domains)

        # check if we pruned down to 0 domains
        if not self.domains or not self.users_owned_domains:
                logging.error("No valid domains were given, to operate on.")
                sys.exit(1)

    def generate_users_owned_domains(self):
        """Builds a dict of users as key, and a list of domains they own, as the values"""
        if self.is_root:
            """WHMAPI doesn't have something that gives us a list of users, and the domains they own. We must build that ourselves using the list of invidual domains/users"""

            get_domain_info = self.api.call(
                "whmapi1",
                "get_domain_info"
                )['domains']

            users = {domain_info['user'] for domain_info in get_domain_info}
            users_owned_domains = {user: set() for user in users}

            for domain_info in get_domain_info:
                user = domain_info['user']
                domain = domain_info['domain']
                # parked domains
                if domain_info['domain_type'] == 'parked':
                    logging.info("Omitting %s as it is an alias of %s." % (domain, domain_info['parent_domain']))
                    continue
                users_owned_domains[user].add(domain)
        else:
            """UAPI's per-user equivalent keeps things simple"""
            get_domain_info = self.api.call(
                "uapi",
                "DomainInfo list_domains",
                self.current_user
                )

            if get_domain_info['parked_domains']:
                logging.info("Omitting %s as they are aliases of %s."
                             % (get_domain_info['parked_domains'],
                                get_domain_info['main_domain']))
            users_domains = set([get_domain_info['main_domain']] \
                + get_domain_info['sub_domains'] \
                + get_domain_info['addon_domains'] \
                )
            users_owned_domains = {self.current_user: users_domains}

        return users_owned_domains

    def prune_users_owned_domains(self, users_owned_domains):
        """Operate on a dict like one generated in generate_users_owned_domains(), narrow down to just cPanel users and given domains"""

        # test if given domains exist in cPanel or not
        for domain in self.domains:
            domain_configed_in_cpanel = any(domain in domains for domains in users_owned_domains.values())
            if not domain_configed_in_cpanel:
                logging.warning("Domain %s does not exist in the list of cPanel domains we can configure!", domain)

        # rebuild with only existing domains
        for user in users_owned_domains:
            for domain in users_owned_domains[user].copy():
                if domain not in self.domains:
                    users_owned_domains[user].remove(domain)

        # omit users with no domains
        no_doms_users = [k for k,v in users_owned_domains.iteritems()
                            if not v]
        for user in no_doms_users:
            del users_owned_domains[user]

        logging.debug("Valid given domains, sorted under the users that own them: %s", users_owned_domains)
        return users_owned_domains