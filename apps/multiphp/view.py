from tabulate import tabulate

def state(text):
    print(text)
def title(text):
    print("---##### %s: #####---" % text)
def subtitle(text):
    print("##### %s: #####" % text)

def table(dataset):
    header = dataset[0].keys()
    rows = [datarow.values() for datarow in dataset]
    print(tabulate(rows, header))

def flat_table(dataset):
    # useful for datasets with \n,
    # since python 2.7 tabulate really doesnt handle those line breaks well
    for row in dataset:
        row_data = row.values()
        subtitle(row_data[0])
        for item in row_data[1:]:
            state(item)
