from argparse import ArgumentParser
from argparse import RawTextHelpFormatter as Raw

class Arg_parser():
    def __init__(self):
        self.epilogs = Epilogs()

        self.primary_parsers()
        self.parent_parsers()
        self.primary_subparsers()
        self.mngr_subparsers()
        self.ini_subparsers()

    def primary_parsers(self):
        self.argparser = ArgumentParser(
            description="cPanel's EasyApache 4 MultiPHP tools, terminal edition",
            prog="multiphp.py",
            epilog=self.epilogs.argparser,
            formatter_class=Raw,
            )
        self.argparser.add_argument(
            '--debug',
            help="print info useful for development debugging",
            action="store_const",
            dest="loglevel",
            const='DEBUG',
            default='WARNING',
            )
        self.argparser.add_argument(
            '--verbose',
            help="print troubleshooting info",
            action="store_const",
            dest="loglevel",
            const='INFO',
            )
        self.argparser.add_argument(
            '--quiet',
            help="don't print non-critical warnings",
            action="store_const",
            dest="loglevel",
            const='ERROR',
            )

    def parent_parsers(self):
        """Subparsers will include these some or all the time"""
        self.dom_parser = ArgumentParser(add_help=False)

        self.dom_parsergroup = \
            self.dom_parser.add_mutually_exclusive_group()
        self.dom_parsergroup.add_argument(
            'domains',
            type=str.lower,
            help="operate on these specified domains",
            nargs='*',
            default=[],
            )
        self.dom_parsergroup.add_argument(
            '-a', '--all',
            help="operate on all domains",
            action='store_true',
            dest='all_domains',
            )

        self.check_parser = ArgumentParser(add_help=False)
        self.check_parser.add_argument(
            '-c', '--check',
            help="run get right after, to see if the changes worked",
            action='store_true',
            )

    def primary_subparsers(self):
        self.primary_subparser = self.argparser.add_subparsers(
            help="choose manager or ini",
            )

        self.mngr_parser = self.primary_subparser.add_parser(
            'manager',
            description="The cPanel MultiPHP Manager, terminal edition.",
            help="see or change PHP versions",
            epilog=self.epilogs.mngr_parser,
            formatter_class=Raw,
            )

        self.ini_parser = self.primary_subparser.add_parser(
            'ini',
            description="The cPanel MultiPHP INI Editor, terminal edition.",
            help="see or change PHP settings",
            epilog=self.epilogs.ini_parser,
            formatter_class=Raw,
            )

    def mngr_subparsers(self):
        self.mngr_subparser = self.mngr_parser.add_subparsers(
            help="choose get or set",
            dest='mngr_subparser',
            )

        self.mngr_get_parser = self.mngr_subparser.add_parser(
            'get',
            help="check PHP versions, utilized handlers, and the source of these settings",
            parents=[self.dom_parser],
            epilog=self.epilogs.mngr_get_parser,
            formatter_class=Raw,
            )

        self.mngr_set_parser = self.mngr_subparser.add_parser(
            'set',
            help="make changes to PHP versions and PHP-FPM settings",
            parents=[self.dom_parser, self.check_parser],
            epilog=self.epilogs.mngr_set_parser,
            formatter_class=Raw,
            )

        self.mngr_set_args()

    def mngr_set_args(self):
        self.mngr_set_parser.add_argument(
            '-v', '--version',
            help="PHP version ID to use, like ea-php## or alt-php##, or just the two ## digits to imply ea-php##, or the \"inherit\" option",
            type=str,
            metavar="php_version_id",
            )

        self.mngr_set_fpmgroup = \
            self.mngr_set_parser.add_mutually_exclusive_group()
        self.mngr_set_fpmgroup.add_argument(
            '--fpm',
            help="enable PHP-FPM, specifying pool variables (requires root)",
            metavar=(
                'max_children',
                'process_idle_timeout',
                'max_requests',
                ),
            nargs=3,
            )
        self.mngr_set_fpmgroup.add_argument(
            '--nofpm',
            help="disable PHP-FPM (requires root)",
            dest='fpm',
            action='store_false',
            default=None,
            )

    def ini_subparsers(self):
        self.ini_subparser = self.ini_parser.add_subparsers(
            help="choose get, set, or edit",
            dest='ini_subparser',
            )

        self.ini_get_parser = self.ini_subparser.add_parser(
            'get',
            help="check PHP settings",
            parents=[self.dom_parser],
            epilog=self.epilogs.ini_get_parser,
            formatter_class=Raw,
            )

        self.ini_set_parser = self.ini_subparser.add_parser(
            'set',
            help="make changes to specified PHP settings",
            parents=[self.dom_parser, self.check_parser],
            epilog=self.epilogs.ini_set_parser,
            formatter_class=Raw,
            )

        self.ini_edit_parser = self.ini_subparser.add_parser(
            'edit',
            help="interactively make changes with $EDITOR to current PHP settings file",
            parents=[self.dom_parser, self.check_parser],
            epilog=self.epilogs.ini_edit_parser,
            formatter_class=Raw,
            )

        self.ini_set_args()

    def ini_set_args(self):
        self.ini_set_parser.add_argument(
            '-s', '--setting',
            help="set a php setting to a given value, can be passed multiple times",
            required=True,
            metavar=(
                'php_setting',
                'value'
                ),
            nargs=2,
            action='append',
            )

    def parse(self):
        return self.argparser.parse_args()

class Epilogs():
    def __init__(self):
        self.argparser = (
            "usage examples:\n"
            "  multiphp.py manager get example.com example2.com\n"
            "  multiphp.py ini edit example.com"
            )
        self.mngr_parser = (
            "usage examples:\n"
            "  multiphp.py manager get example.com \n"
            "  multiphp.py manager set example.com -v 73"
            )

        self.mngr_get_parser = (
            "usage examples:\n"
            "  multiphp.py manager get example.com\n"
            "  multiphp.py manager get -a\n"
            "  multiphp.py manager get example.com example2.com example3.com"
            )

        self.mngr_set_parser = (
            "usage examples:\n"
            "  multiphp.py manager set example.com -v 73\n"
            "  multiphp.py manager set example.com example2.com example3.com -v inherit --nofpm\n"
            "  multiphp.py manager set -a --fpm 5 10 20\n"
            "  multiphp.py manager set example.com -v ea-php72 --fpm 50 25 100"
            )

        self.ini_parser = (
            "usage examples:\n"
            "  multiphp.py ini get example.com example2.com\n"
            "  multiphp.py ini set -a -s memory_limit 128M\n"
            "  multiphp.py ini edit example.com"
            )

        self.ini_get_parser = (
            "usage examples:\n"
            "  multiphp.py ini get example.com\n"
            "  multiphp.py ini get example.com example2.com\n"
            "  multiphp.py ini get -a"
            )
        self.ini_set_parser = (
            "usage examples:\n"
            "  multiphp.py ini set example.com example2.com -s memory_limit 128M\n"
            "  multiphp.py ini set -a -s post_max_size 256M -s upload_max_filesize 256M"
            )

        self.ini_edit_parser = (
            "usage examples:\n"
            "  multiphp.py ini edit example.com\n"
            "  multiphp.py ini edit -a"
            )
