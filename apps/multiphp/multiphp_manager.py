import sys
import logging
from collections import OrderedDict

import multiphp

class Manager(multiphp.MultiPHP):
    def get(self):
        def process_user(user, user_vhosts_php, known_php_handlers={}):
            return_table_user = []
            for vhost in user_vhosts_php:
                if vhost['vhost'] not in self.users_owned_domains[user]:
                    continue
                row = OrderedDict()
                row['User'] = user
                row['Domain'] = vhost['vhost']
                # row['Handler'] =
                if vhost['php_fpm'] == 0:
                    if vhost['version'] not in known_php_handlers:
                        known_php_handlers[vhost['version']] = self.api.call(
                            'uapi',
                            'LangPHP php_get_domain_handler type=vhost '
                            'vhost=%s' % vhost['vhost'])['php_handler']

                    row['Handler'] = known_php_handlers[vhost['version']]
                else:
                    row['Handler'] = "php-fpm (%s/%s/%s)" % (
                        vhost['php_fpm_pool_parms']['pm_max_children'],
                        vhost['php_fpm_pool_parms']['pm_process_idle_timeout'],
                        vhost['php_fpm_pool_parms']['pm_max_requests'],
                        )
                row['PHP Version'] = vhost['version']
                # row['Inheritance'] =
                if "system_default" in vhost['phpversion_source']:
                    row['Inheritance'] = "System Default"
                elif vhost['vhost'] == vhost['phpversion_source']['domain']:
                    row['Inheritance'] = "Explicitly Set"
                else:
                    row['Inheritance'] = vhost['phpversion_source']['domain']

                return_table_user.append(row)
            return return_table_user

        return_table = []
        if self.is_root:
            version_handlers = self.api.call(
                'whmapi1','php_get_handlers')['version_handlers']
            known_php_handlers = {}
            for version in version_handlers:
                known_php_handlers[version['version']] = version['current_handler']

            for user in self.users_owned_domains:
                user_vhosts_php = self.api.call("uapi",
                    "LangPHP php_get_vhost_versions", user)
                return_table += process_user(user, user_vhosts_php,
                                             known_php_handlers)
        else:
            user_vhosts_php = self.api.call("uapi",
                "LangPHP php_get_vhost_versions")
            user = self.current_user
            return_table += process_user(user, user_vhosts_php)

        return return_table

    def set(self, version, fpm):
        def handle_pver():
            if version is None:
                return ''

            # if user gave us digits, prefix ea-php, else we assume the user gave a full php ID.
            try:
                php_id = "ea-php" + str(int(version))
            except ValueError:
                php_id = version

            if self.is_root:
                inst_php_vers = self.api.call(
                    'whmapi1', 'php_get_installed_versions')['versions']
            else:
                inst_php_vers = self.api.call(
                    'uapi', 'LangPHP php_get_installed_versions')['versions']
            logging.info("Installed PHP versions: %s", inst_php_vers)

            if php_id in inst_php_vers or php_id == 'inherit':
                return 'version=' + php_id
            else:
                logging.critical(
                    "Provided PHP version %s is not installed. "
                    "Currently installed:\n%s",
                    php_id, '\n'.join(inst_php_vers))
                sys.exit(1)

        def handle_fpm():
            if fpm is None:
                return ''
            if not self.is_root:
                logging.critical("PHP-FPM cannot be modified without root access")
                sys.exit(1)

            if isinstance(fpm, (list,)):
                # fpm + php version to inherit, is bad
                if version == "inherit":
                    logging.warning("PHP-FPM cannot be enabled while also setting PHP version to \"inherit\", skipping this request")
                    return ''
                if version is None:
                    logging.warning("Keep in mind that PHP-FPM will fail to enable if the PHP version is set to \"inherit\". This script doesnt check for that, hopefully you did.")
                return 'php_fpm=1 php_fpm_pool_parms={\'' \
                    + '"pm_max_children":' + fpm[0] \
                    + ',"pm_process_idle_timeout":' + fpm[1] \
                    + ',"pm_max_requests":' + fpm[2] \
                    + '}\''
            elif fpm is False:
                return 'php_fpm=0'

        if fpm is None and version is None:
            logging.error("With the set command, please specify a PHP version and/or PHP-FPM.")
            sys.exit(1)

        cmd = []
        if self.is_root:
            cmd_api = 'whmapi1'
            cmd.append('php_set_vhost_versions')
            domains = []
            for user in self.users_owned_domains:
                domains += self.users_owned_domains[user]
        else:
            cmd_api = 'uapi'
            cmd.append('LangPHP php_set_vhost_versions')
            domains = self.users_owned_domains[self.current_user]

        cmd.append(handle_pver())
        cmd.append(handle_fpm())

        for domain in domains:
            cmd.append("vhost=" + domain)

        self.api.call(cmd_api, " ".join(cmd))
        return True