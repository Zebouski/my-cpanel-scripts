'''cPanels EasyApache 4 MultiPHP tools, terminal edition.'''
import logging
import sys

import arg_parser
import multiphp_manager
import multiphp_ini
import view

parser = arg_parser.Arg_parser()
args = parser.parse()

logging.basicConfig(
    format='[ %(levelname)s ] %(message)s ',
    stream=sys.stdout,
    level=args.loglevel
    )
logger = logging.getLogger()

logging.debug("Parsed Args: %s", args)

# todo: might be a way to handle this in argparse instead
if args.all_domains:
    args.domains = True

if hasattr(args, 'mngr_subparser'):
    manager = multiphp_manager.Manager(args.domains)

    if args.mngr_subparser == 'set':
        manager.set(args.version, args.fpm)
        view.state("Changes completed.")
        if args.check:
            args.mngr_subparser = 'get'
    if args.mngr_subparser == 'get':
        view.title("PHP Versions")
        view.table(manager.get())

elif hasattr(args, 'ini_subparser'):
    ini = multiphp_ini.Ini(args.domains)

    if args.ini_subparser == 'edit':
        ini.edit()
        view.state("Changes completed.")
        if args.check:
            args.ini_subparser = 'get'
    if args.ini_subparser == 'set':
        ini.set(args.setting)
        view.state("Changes completed.")
        if args.check:
            args.ini_subparser = 'get'
    if args.ini_subparser == 'get':
        view.title("PHP Settings")
        view.flat_table(ini.get())
