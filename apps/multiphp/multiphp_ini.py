import os
import subprocess
import tempfile
import urllib
from getpass import getuser
import logging
import multiphp
from collections import OrderedDict
import shlex


class Ini(multiphp.MultiPHP):
    def unescape(self, s):
         s = s.replace('&lt;', '<')
         s = s.replace('&gt;', '>')
         s = s.replace('&quot;', '"')
         s = s.replace('&amp;', '&') # this has to be last
         return s

    def get(self):
        return_table = []
        cmd_api = 'uapi'
        cmd = "LangPHP php_ini_get_user_content type=vhost"
        for user in self.users_owned_domains:
            cmd_user = user if self.is_root else None

            for domain in self.users_owned_domains[user]:
                row = OrderedDict()

                row["Domain"] = domain

                # skip if we encounter a failure like a technical subdomain
                try:
                    php_settings = self.api.call(
                        cmd_api,
                        "{0} vhost={1}".format(cmd, domain),
                        cmd_user,
                        )['content']
                except:
                    continue

                php_settings = self.unescape(php_settings)
                row["PHP Settings"] = php_settings

                return_table.append(row)
        return return_table

    def set(self, settings):
        cmd_api = 'uapi'
        cmd = "LangPHP php_ini_set_user_basic_directives type=vhost"
        for user in self.users_owned_domains:
            cmd_user = user if self.is_root else None

            for domain in self.users_owned_domains[user]:
                cmd_settings = []
                for index, setting in enumerate(settings, start=1):
                    cmd_settings.append(
                        "directive-{0}={1}%3A{2}".format(
                            index, setting[0], setting[1])
                        )
                self.api.call(
                    cmd_api,
                    "{0} vhost={1} {2}".format(
                        cmd, domain, " ".join(cmd_settings)),
                    cmd_user,
                    )
        return True

    def edit(self):
        cmd_api = 'uapi'
        for user in self.users_owned_domains:
            cmd_user = user if self.is_root else None

            for domain in self.users_owned_domains[user]:
                # get ini content
                php_ini_get = self.api.call(
                    cmd_api,
                    "LangPHP php_ini_get_user_content type=vhost vhost={0}".format(domain),
                    cmd_user,
                    )

                # populate tempfile with content
                try:
                    ini_temp = tempfile.NamedTemporaryFile(
                        prefix='multiphp_{0}_'.format(domain),
                        suffix=".tmp",)
                    ini_temp.write(self.unescape(php_ini_get['content']))
                    ini_temp.flush()
                except:
                    # we have no contents to work on, ini get has failed
                    continue

                # open tempfile with interactive editor
                subprocess.call([os.environ.get('EDITOR', 'nano'),
                                 ini_temp.name])

                # save changes
                ini_temp.seek(0)
                uri_encoded_contents = urllib.quote(ini_temp.read(),
                                                    safe='')
                self.api.call(
                    cmd_api,
                    'LangPHP php_ini_set_user_content type=vhost vhost={0} content={1}'.format(domain, uri_encoded_contents),
                    cmd_user,
                    )
        return True
