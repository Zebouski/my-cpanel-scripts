#!/usr/bin/env bash
function logscreen_failure () {
    echo 'Could not enable screen logging! Ensure your terminal has tier1adv access or higher, or escalate. See also: https://wiki.inmotionhosting.com/index.php?title=GNU_Screen'
    exit 1
}

if [[ -w /home/t1bin ]]; then
  screen_log_path="/home/t1bin"
elif [[ -w /home/tier1adv ]]; then
  screen_log_path="/home/tier1adv"
else
  logscreen_failure
fi

if [[ $USER =~ ^(tier1adv|root)$ ]]; then
    read -ep 'Your IMH Agent Name: ' agent_name
else
    agent_name="$USER"
fi

screen_name="$(date -Is).${agent_name}"
screen_log="${screen_log_path}/${screen_name}"
screen_rc="$(mktemp)"

# bash interpreted screenrc commands
cat > ${screen_rc} << EOF
  sessionname ${screen_name}
  logfile ${screen_log}.%n
EOF

# literal screenrc commands
cat >> ${screen_rc} << 'EOF'
  logfile flush 2 # update the log every 2 seconds instead of 10
  deflog on

  layout new

  # change escape key to grave
  escape ``

  screen -t Admin # for root/tier1adv terminals
  split
  focus down
  screen -t User # for cPanel user terminals
  focus up

  # fixes fullscreen TUI applications leaving residual output
  altscreen on

  # bottom bar with Hostname, Screen name, Window list, Server load, and current Date/Time
  backtick 0 30 30 sh -c 'screen -ls | grep --color=no -o "$PPID[^[:space:]]*"' # find the name of the screen we are in
  hardstatus alwayslastline
  hardstatus string '[ %H ] [ %` ] [ %= %{-} %-Lw%{= bW}%n%f %t%{-}%+Lw %= ] [ Load: %l ] [%Y-%m-%dT%0c:%s]'
EOF

\screen -c ${screen_rc}

for log in ${screen_log}.*; do
  if [[ ! -e ${log} ]]; then
    logscreen_failure
  else
    echo "Screen logging was enabled, agents can watch with:"
    echo "tail -f ${screen_log}.*"
  fi
  break
done
