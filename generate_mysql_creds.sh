#!/usr/bin/env bash
set -e

if [[ $EUID -eq 0 ]]; then
    echo "This script must not be run as root, but instead as the cPanel user you wish to work on."
    exit 1
fi

mysql_restrictions="$(mktemp)"
uapi Mysql get_restrictions > ${mysql_restrictions}

# determine what we need to prefix db and username with
mysql_prefix="$(awk '$1 == "prefix:" {print $2}' ${mysql_restrictions})"

# build db and username based on prefix, and shorten to max allowed lengths
dbname="${mysql_prefix}$(cut -c1-$(awk '$1 == "max_database_name_length:" {print $2}' ${mysql_restrictions}) <<< $1)"
dbuser="${mysql_prefix}$(cut -c1-$(awk '$1 == "max_username_length:" {print $2}' ${mysql_restrictions}) <<< $2)"
# if no pass provided, randomize
dbpass="${3:-$( openssl rand -base64 12 | tr -cd '[:alnum:]' )}"

if ! [[ ${dbpass} =~ ^[[:alnum:]]*$ ]]; then
  echo -e "\e[31mPassword is not alphanumeric, issues setting password may be encountered.\e[39m"
fi

echo -e "For cPanel user ${USER}, creating MySQL database \e[33m${dbname}\e[39m managed by MySQL username \e[33m${dbuser}\e[39m using password \e[33m${dbpass}\e[39m using these uapi commands:"

return="$(mktemp)"
(
  set -x # print commands we run
  uapi Mysql create_database name="${dbname}"
  uapi Mysql create_user name="${dbuser}" password="${dbpass}"
  uapi Mysql set_privileges_on_database database="${dbname}" user="${dbuser}" privileges="ALL PRIVILEGES"
) 2> /dev/null > ${return} # capture output of these uapi comands for later

# test creds to see if they are working
if mysql -p${dbpass} -u ${dbuser} ${dbname} -e ";" 2> /dev/null; then
  echo -e "\e[32mGenerated MySQL credentials confirmed working.\e[39m"
else
  echo -e "\e[31mGenerated MySQL credentials not working, errors from uapi:\e[39m"

  # only print everything between "errors:" and "messages:"
  awk 'P && /messages:/ { P=0 }; P; /errors:/ { P=1 }' ${return}
fi
